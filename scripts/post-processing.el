;;; post-processing --- Summary

;;; Commentary;
;;; Post processing preparation for PDF

(defun remove-orgmode-latex-labels (filename)
  "Remove labels generated by org-mode"
  (interactive "fFile to hack on: ")
  (with-current-buffer (find-file filename)
    (let ((case-fold-search nil))
      (goto-char (point-min))
      (replace-regexp "^\\\\label{sec-[0-9][^}]*}\n" "")
      )))

(defun org-to-latex-toc (filename)
  "Map org sections to LaTeX ToC."
  (interactive "fFile to hack on: ")
  (with-current-buffer (find-file filename)
    (let ((case-fold-search nil))
      (goto-char (point-min))
      (while (re-search-forward  (rx bol "\\section") (point-max) t)
        (replace-match "\\chapter" nil t))
      (goto-char (point-min))
      (while (re-search-forward (rx bol "\\subsection") (point-max) t)
        (replace-match "\\section" nil t))
      (goto-char (point-min))
      (while (re-search-forward (rx bol "\\subsubsection") (point-max) t)
        (replace-match "\\subsection" nil t))
      )))

(defun italicize-verse (filename)
  "Italicize verse."
  (interactive "fFile to hack on: ")
  (with-current-buffer (find-file filename)
    (let ((case-fold-search nil))
      (goto-char (point-min))
      (while (re-search-forward (rx bol "\\begin{verse}") (point-max) t)
        (replace-match "\\emph{\\begin{verse}[\\versewidth]" nil t))
      (goto-char (point-min))
      (while (re-search-forward (rx bol "\\end{verse}") (point-max) t)
        (replace-match "\\end{verse}}" nil t))
      ))
  )

(defun italicize-quote (filename)
  "Italicize quotes."
  (interactive "fFile to hack on: ")
  (with-current-buffer (find-file filename)
    (let ((case-fold-search nil))
      (goto-char (point-min))
      (while (re-search-forward (rx bol "\\begin{quote}") (point-max) t)
        (replace-match "\\emph{\\begin{quote}" nil t))
      (goto-char (point-min))
      (while (re-search-forward (rx bol "\\end{quote}") (point-max) t)
        (replace-match "\\end{quote}}" nil t))
      ))
  )

(defun post-process (filename)
  "Post-processing hacks."
  (interactive "fFile to hack on: ")
  (with-current-buffer (find-file filename)
    (remove-orgmode-latex-labels filename)
    (org-to-latex-toc filename)
    (italicize-verse filename)
    (italicize-quote filename))
    )

(provide 'post-processing)
